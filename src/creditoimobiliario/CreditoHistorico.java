package creditoimobiliario;

public class CreditoHistorico {

	private String tipoDoImovel, localizacaoUF, aprovado;
	private int mesesAdiquirir, prazoEmAnos;
	private double valorDoImovel, valorFinanciado, rendaMensal;
	CreditoHistorico[] historicoVect = new CreditoHistorico[4];	

	/**
	 * Contrutor padr�o
	 */
	public CreditoHistorico() {
		// TODO Auto-generated constructor stub
	}	
	/**
	 * Construtor com parametros
	 * @param tipoDoImovel
	 * @param localizacaoUF
	 * @param mesesAdiquirir
	 * @param prazoEmAnos
	 * @param valorDoImovel
	 * @param valorFinanciado
	 * @param rendaMensal
	 * return Metodo contrutor
	 */
	public CreditoHistorico(String tipoDoImovel, String localizacaoUF, String aprovado, int mesesAdiquirir, int prazoEmAnos,
			double valorDoImovel, double valorFinanciado, double rendaMensal) {		
		this.tipoDoImovel = tipoDoImovel;
		this.localizacaoUF = localizacaoUF;
		this.aprovado = aprovado;
		this.mesesAdiquirir = mesesAdiquirir;
		this.prazoEmAnos = prazoEmAnos;
		this.valorDoImovel = valorDoImovel;
		this.valorFinanciado = valorFinanciado;
		this.rendaMensal = rendaMensal;
	}

	/**
	 * 
	 * @return metodos especiais
	 */
	public String getTipoDoImovel() {
		return tipoDoImovel;
	}
	public void setTipoDoImovel(String tipoDoImovel) {
		this.tipoDoImovel = tipoDoImovel;
	}

	public String getLocalizacaoUF() {
		return localizacaoUF;
	}
	public void setLocalizacaoUF(String localizacaoUF) {
		this.localizacaoUF = localizacaoUF;
	}

	public int getMesesAdiquirir() {
		return mesesAdiquirir;
	}
	public void setMesesAdiquirir(int mesesAdiquirir) {
		this.mesesAdiquirir = mesesAdiquirir;
	}

	public int getPrazoEmAnos() {
		return prazoEmAnos;
	}
	public void setPrazoEmAnos(int prazoEmAnos) {
		this.prazoEmAnos = prazoEmAnos;
	}

	public double getValorDoImovel() {
		return valorDoImovel;
	}
	public void setValorDoImovel(double valorDoImovel) {
		this.valorDoImovel = valorDoImovel;
	}

	public double getValorFinanciado() {
		return valorFinanciado;
	}
	public void setValorFinanciado(double valorFinanciado) {
		this.valorFinanciado = valorFinanciado;
	}

	public double getRendaMensal() {
		return rendaMensal;
	}
	public void setRendaMensal(double rendaMensal) {
		this.rendaMensal = rendaMensal;
	}
	
	public String getAprovado() {
		return aprovado;
	}
	public void setAprovado(String aprovado) {
		this.aprovado = aprovado;
	}
	
	@Override
	public String toString() {
		return "CreditoHistorico [tipoDoImovel=" + tipoDoImovel + ", localizacaoUF=" + localizacaoUF
				+ ", mesesAdiquirir=" + mesesAdiquirir + ", prazoEmAnos=" + prazoEmAnos + ", valorDoImovel="
				+ valorDoImovel + ", valorFinanciado=" + valorFinanciado + ", rendaMensal=" + rendaMensal + "]";
	}

}
