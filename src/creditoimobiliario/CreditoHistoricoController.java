package creditoimobiliario;

public class CreditoHistoricoController {
	CreditoHistorico credito;
	
	
	public CreditoHistoricoController(CreditoHistorico credito) {		
		this.credito = credito;
	}
	
//	CreditoHistoricoController controller = new CreditoHistoricoController(credito);
	
	/**
	 * @param historicoVect
	 * @return O metodo usa um loop para percorrer todos o objetos da array de
	 * historico de solicitacoes de credito, e invoca os metodos toString() e 
	 * o aprovar() da classe CreditoHistorico, exibindo na tela todos os itens
	 * da array de dizendo se suas solicitacoes de creditos foram aprovadas 
	 * ou n�o.
	 */
	public String[][] showAllThatSweetInformation(CreditoHistorico historicoVect[]) {
		String[] toString = new String[historicoVect.length];
		String[] aprovar = new String[historicoVect.length];		
		
		for(int i = 0; i<historicoVect.length; i++) {
			
			toString[i] = historicoVect[i].toString();
			aprovar[i] = historicoVect[i].getAprovado();	 		
			
		}		
		String[][] matrix = {toString, aprovar}; 
		return matrix;
	}
	/**
	 * @return Metodo que compara o valor do imovel em rela��o ao valor a ser
	 * financiado e ao salario da pessoa, e retorna se o cr�dito foi aprovado
	 * ou n�o
	 */
	public String aprovar(CreditoHistorico credito) { 
		if(credito.getValorDoImovel() >= credito.getValorFinanciado() 
				&& credito.getRendaMensal() >= (credito.getValorDoImovel()/50)) {
			credito.setAprovado("Cr�dito aprovado");
			return credito.getAprovado();			
		}else {
			credito.setAprovado("Cr�dito reprovado");
			return credito.getAprovado();
		}
	}
}
