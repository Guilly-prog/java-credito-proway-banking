package creditoimobiliario;


import org.junit.Assert;
import org.junit.jupiter.api.Test;

class CreditoHistoricoTest {

	
	@Test
	void MetodoAprovadoRetornaCreditoAprovado() {
CreditoHistorico historico = new CreditoHistorico();
		
		historico.setValorDoImovel(500000);
		historico.setValorFinanciado(400000);
		historico.setRendaMensal(15000);
		
		CreditoHistoricoController controller = new CreditoHistoricoController(historico);		
		
		String test = controller.aprovar(historico);
		
		Assert.assertEquals("Cr�dito aprovado", test);
	}
	
	@Test
	void MetodoAprovadoRetornaCreditoReprovado() {
		CreditoHistorico historico = new CreditoHistorico();
		
		historico.setValorDoImovel(300000);
		historico.setValorFinanciado(400000);
		historico.setRendaMensal(5000);
		
		CreditoHistoricoController controller = new CreditoHistoricoController(historico);		
		
		String test = controller.aprovar(historico);
		
		Assert.assertEquals("Cr�dito reprovado", test);
	}
	
	@Test
	void MetodoToString() {	
		
		String tipoDoImovel = "casa";
		String localizacaoUF = "sp";
		String aprovado = "Cr�dito reprovado";
		int mesesAdiquirir = 10;
		int prazoEmAnos = 5;
		double valorDoImovel = 500000;
		double valorFinanciado = 400000;
		double rendaMensal = 899;
		
		
		CreditoHistorico historico = new CreditoHistorico(tipoDoImovel, localizacaoUF, aprovado, mesesAdiquirir, prazoEmAnos,
				valorDoImovel, valorFinanciado, rendaMensal);
		
		Assert.assertEquals("CreditoHistorico [tipoDoImovel=" + tipoDoImovel + ", localizacaoUF=" + localizacaoUF
				+ ", mesesAdiquirir=" + mesesAdiquirir + ", prazoEmAnos=" + prazoEmAnos + ", valorDoImovel="
				+ valorDoImovel + ", valorFinanciado=" + valorFinanciado + ", rendaMensal=" + rendaMensal + "]", historico.toString()
				);
	}
	
	@Test
	void showAllThatSweetInformation() {
		CreditoHistorico historico = new CreditoHistorico();			
		CreditoHistorico[] historicoVect = new CreditoHistorico[1];	
		
		String[] toString = new String[historicoVect.length];
		String[] aprovar = new String[historicoVect.length];				
		
		historicoVect[0] = historico;
		
		historicoVect[0].setTipoDoImovel("casa");
		historicoVect[0].setLocalizacaoUF("SP");
		historicoVect[0].setAprovado(null);
		historicoVect[0].setMesesAdiquirir(10);
		historicoVect[0].setPrazoEmAnos(5);
		historicoVect[0].setValorDoImovel(500000);
		historicoVect[0].setValorFinanciado(400000);
		historicoVect[0].setRendaMensal(10000);
		
		CreditoHistoricoController controller = new CreditoHistoricoController(historico);
		controller.aprovar(historico);
			
		String[][] matrix2 = controller.showAllThatSweetInformation(historicoVect);
		Assert.assertEquals("CreditoHistorico [tipoDoImovel=casa, localizacaoUF=SP, mesesAdiquirir=10, prazoEmAnos=5, "
				+ "valorDoImovel=500000.0, valorFinanciado=400000.0, rendaMensal=10000.0]", matrix2[0][0]);
	}
	
}
