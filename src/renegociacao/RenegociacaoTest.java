package renegociacao;


import org.junit.Assert;
import org.junit.jupiter.api.Test;

class RenegociacaoTest {
/**
 * construtor referente a somente um m�s, dando o desconto de 5%.
 * 
 * @author Cleber Santos
 * 
 */
	@Test
	void  valorDaParcelaParaUmMes() {
		renegociacao teste = new renegociacao(); //  costrutor ( construindo um obj )
		
		/**
		 * Numero dentro das chaves  representando o valor mensal  e o m�s.
		 * 
		 * @author Cleber Santos
		 *  
s		 */
		Double total = teste.valorMensal(500,1);//numero dentro das chaves para rodar os testes ( PARAMETROS ( VALOR|mENSAL E MESES ))
		Assert.assertEquals(475,total,000.1); // valor com desconto.
		
	}
	/**
	 * construtor referente a meses acima de 1, acrescentando juros de 5%.
	 * 
	 * Double total = teste.valorMensal(600,5) valor passado como referencia. 
	 * Assert.assertEquals(630,total,000.1) valor for�ando o erro para teste.
	 * 
	 * @author Cleber Santos
	 * 
	 */
	@Test
	void valorDaParcelaParaMaisDeUmMes() {
		renegociacao teste = new renegociacao();
		Double total = teste.valorMensal(600,5);//numero dentro das chaves para rodar os testes
		Assert.assertEquals(630,total,000.1);
		 
	}

}
