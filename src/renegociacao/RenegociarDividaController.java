package renegociacao;

public class RenegociarDividaController {
	RenegociarDivida divida;  // instancia��o
	
	
	/** 
	 * double parcelasDivida = divida.getValorDivida()/divida.getParcelas(); pega os valores citados ( divida e parcelas )
	 * @author Cleber Santos
	 * @return
	 */ 
	public Double calcularDivida() {        // metodo
		
		double total = 0; 
		double parcelasDivida = divida.getValorDivida()/divida.getParcelas();
		
		/**
		 * Metodo que pega o m�s e acrescenta + 1,  no total pega a parcela e coloca o juros de 5% 
		   ( para o acrescimo de juros o m�s tem que ser superior a 1
		   
		 * @author Cleber Santos
		 */ 
	      if (divida.getParcelas() >1 ) {
	        for (int i=0; i<divida.getParcelas();i++) {
	        	total += parcelasDivida + (parcelasDivida*divida.getTaxaJuros());  
	        }
	         
	        /**
	         * Metodo que pega o mes igual a 1 e da desconto de 5% ( total = parcelasDivida*0.95 ).
	         */
	      }else if (divida.getParcelas() == 1) { 
	          total = parcelasDivida*0.95;    // esta dando desconto
	      }
	      
	      /**
	       * Return esta retornando o total adequado conforme escolha acima, se � 1 m�s ter� o desconto ou, se for mais de um
	         mes acrescentar� o juros .
	       */
	      return total;
	         
		
	}
}   
